# Manual Steps to setup a server

1. Install Amazon's version of java

    ```bash
    sudo rpm --import https://yum.corretto.aws/corretto.key
    sudo curl -L -o /etc/yum.repos.d/corretto.repo https://yum.corretto.aws/corretto.repo
    sudo yum install -y java-16-amazon-corretto-devel
    ```

2. Pre-Reqs for Minecraft

    ```bash
    useradd --system --user-group --create-home -K UMASK=0022 --home /opt/minecraft/ minecraft
    mkdir -P /opt/minecraft/server/
    ```

3. Download minecraft

    ```bash
    wget https://launcher.mojang.com/v1/objects/c8f83c5655308435b3dcf03c06d9fe8740a77469/server.jar -P /opt/minecraft
    chown -R minecraft:minecraft
    ```

4. Create a eula file

    ```bash
    echo "eula=true" >> eula.txt
    ```

5. GOSHIT
