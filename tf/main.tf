terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}

resource "aws_ec2_host" "MC" {
  instance_type     = "c5.18xlarge"
  availability_zone = "us-east-1"
  host_recovery     = "on"
  auto_placement    = "on"
}