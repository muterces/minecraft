% MC(1)
% Alec Carpenter
% March 2022

# Name

mc - Minecraft server manager written in go

# Synopsis

*mc* [-v|--version] [-h|--help] [-c|--create <world> <port> <ip>]
     [-d|--delete <world>] [-s|--start <world>] [-r|--restart <world>] 
     [-S|--stop <world>] [-e|--enable <world>] [-l|--list]
     [-i|--import <directory ><world> <port> <ip>] [-b|--backup <world>]
     [-R|--rename <original world> <new world>] [--list-backups]
     [--restore-backup <name>] [-B|--broadcast <command>]
     [--send <world> <command>] [-C|--console <world>] 
     [--status <world>] [-u|--update <world>] [--restore-backup <name>]
     <command> [<args]
  
# Description

**mc** allows an adminstator to fully manage a minecraft server with a few simple commands. **mc** can create, delete, restart, stop, start, import, rename, enable, and list minecraft worlds. Manage backups using **mc** to start, list, or restore a backup. **mc** can also send commands to minecraft worlds and manage minecraft logs.

# Options

**-h**, **--help**
: Displays the help page

**-v**, **--version**
: Displays the version of mc

**c**, **--create**

**-d**, **--delete**

**-s**, **--start**

**-r**, **--restart**

**-S**, **--stop**

**-e**, **--enable**

**-l**, **--list**

**-i**, **--import**

**-R**, **--rename**

**-b**, **--backup**

**--restore-backu**

**--list-backups**

**-B**, **--broadcast**

**--send**

**-C**, **--console**

**--status**

**-u**, **--update**

# Examples

# Exit Values

# Bugs

# Author

# Copyright
